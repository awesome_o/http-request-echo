# http-request-echo

![logo.png](logo.png)

#### description

A simple `crystal-lang` app to echo / inspect `HTTP requests`

Inspired by https://httpbin.org/anything

#### local

    crystal http-request-echo.cr

_or_

    DEBUG=true crystal http-request-echo.cr

_**requires**_
- `crystal` : https://crystal-lang.org

#### hot-reload

watches for changes in the first matched `.cr` file and reloads it as soon as it gets updated

    sh hot-reload.sh

_**requires**_
- `entr` : http://eradman.com/entrproject

#### update dependencies

    shards install

#### build binary

    shards build

#### run binary

    bin/http-request-echo

##### mockup-http-requests

Generate configurable mockup HTTP requests w/ random query_params & payload

    sh mockup-http-requests.sh

_**requires**_
- `jq` : https://stedolan.github.io/jq
- `/usr/share/dict/words`
- https://random-data-api.com

#### tunnel

this is useful for using locally running code _(or compiled binary)_ as a `webhook` receiver for instance

    ngrok http 8080

_or_

    ngrok http --log stdout --log-format json --region eu 8080  | jq -r '.url? | select( . | contains( "https://" )? )?'

_**requires**_
- `ngrok` : https://ngrok.com
- `jq`    : https://stedolan.github.io/jq

#### heroku

https://http-request-echo.herokuapp.com

#### reference

reference docs & online playgrounds

- `bash`         : https://tldp.org/LDP/abs/html
- `crystal-lang` : https://play.crystal-lang.org
- `heroku`       : https://github.com/crystal-lang/heroku-buildpack-crystal
- `jq`           : https://jqplay.org
