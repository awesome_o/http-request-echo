require "http/server"
require "json"

DEBUG = ENV["DEBUG"] ||= "false"

bind  = ENV["BIND"]  ||= "0.0.0.0"
port  = ENV["PORT"]  ||= "8080"
port  = port.to_i

server = HTTP::Server.new do |http|
    http.response.print request_parser(http)
    http.response.close
end

address = server.bind_tcp bind, port
server.listen

def request_parser(*args)

    request = args.first.request

    method  = request.method.to_s
    path    = request.path.to_s
    params  = request.query_params.to_h
    headers = request.headers.to_h

    flat_headers = {} of String => String
    headers.each { |header|
        flat_header = {"#{header.first}" => "#{header.last.join(",")}"}
        flat_headers.merge!(flat_header)
    }
    headers = flat_headers.to_a.sort_by! { |key, value| key }.to_h

    host    = headers["Host"]

    errors  = nil
    body    = parse_body(args.first)

    request =
    {
        "http" =>                \
        {
            "method"  => method  ,
            "host"    => host    ,
            "path"    => path    ,
            "params"  => params  ,
            "headers" => headers ,
            "body"    => body
        }
    }.to_pretty_json

    DEBUG == "true" && STDERR.puts request

    return request

end

def parse_body(*args)

    request = args.first.request

    body    = request.body
    headers = request.headers.to_h

    if body.nil?
        body = nil
    else
        body = body.as(IO).gets_to_end.to_s
        begin
            body = JSON.parse(body)
        rescue
            true
        end
    end

    return body

end
