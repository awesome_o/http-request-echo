#!/bin/sh
# hot-reload app as soon as it gets updated
script=${script:-$(ls -1 *.cr|head -1)}
echo ${script} 1>&2
ls ${script} | entr -r crystal ${script}
