require "kemal"

before_all    { |http| http.response.content_type = "application/json" }

get     "/*"  { |http| echo http }
put     "/*"  { |http| echo http }
post    "/*"  { |http| echo http }
patch   "/*"  { |http| echo http }
options "/*"  { |http| echo http }
delete  "/*"  { |http| echo http }

error 404     { |http|
    http.response.content_type = "application/json"
    r = http.request
    {error: 404, message:"no such route", method: "#{r.method}", path: "#{r.path}"}.to_json
}

Kemal.run

def echo(http : HTTP::Server::Context)
    begin
        r = http.request
        p = http.params
        h = {
            body:     p.json            ,
            host:     r.headers["Host"] ,
            path:     r.path            ,
            query:    p.query.to_h      ,
            method:   r.method          ,
            headers:  r.headers         ,
            version:  r.version         ,
            resource: r.resource
        }
        p.query.has_key?("pretty") && h.to_pretty_json || h.to_json
    rescue error
        STDERR.puts error
        http.response.status_code = 400
        {error:error.inspect}.to_json
    end
end

