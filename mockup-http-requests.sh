#!/bin/sh

set -euo pipefail

verb()       { verbs="GET\nPOST\nPUT\nPATCH\nOPTIONS\n" ; printf "${verbs}" | shuf -n1 ; }

word()       { shuf -n1 /usr/share/dict/words ; }
words()      { echo "$(word)-$(word)" ; }

endpoint()   { printf "${endpoints}" | grep "${1:-random}" | shuf -n1 ; }
payload()    { curl -s "$( endpoint "${1:-random}" )" ; }

params()     { payload | jq -r '[ to_entries[] | "\( .key )=\( .value | @uri )" ] | join( "&" )' ; }

data_types() { echo "$endpoints" | xargs basename | grep ^random_ | cut -d '_' -f2- | sort ; }

writeout() {
    cat << HEREDOC
\n%{stderr}{"http_code":%{http_code},"time_total":%{time_total},"url_effective":%{url_effective}}\n
HEREDOC
}

curl() {
    /usr/bin/curl                 \
        --silent                  \
        --insecure                \
        --location                \
        --write-out "\n"          \
        --user-agent "${user_agent:-$( words )}" \
        "${@}"
}

endpoints=$( curl -s "https://random-data-api.com/documentation" | grep 'href="https://random-data-api.com/api/' | cut -d'"' -f2 | sort | uniq )

if [[ "${1:-}" == "--help" ]] ; then data_types ; exit ; fi

while sleep ${delay:-0} ; do
    curl "${url:-127.0.0.1:8080}${params:-?$(params)}"       \
        -H "Content-Type: ${content_type:-application/json}" \
        -X "${verb:-$(verb)}"                                \
        -d "${payload:-$(payload "${data:-random}")}"        \
        -w "${writeout:-$(writeout)}"
done
